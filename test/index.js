/* globals describe, before, beforeEach, it */

'use strict';

describe('#amqp.rpc', function() {
  var nigah = require('nigah');
  var expect = require('chai').expect;
  var Promise = require('bluebird');

  var amqp = require('amqplib');
  var rpc = require('./../src');

  var client, server, watcher;

  var api = {
    add: function(one, two) {
      this.resolve(one + two);
    },
    json: function(j) {
      this.resolve(j);
    },
    args: function(a, b, c) {
      this.resolve([a, b, c]);
    },
    reject: function(err) {
      this.reject(new Error(err || 'remote error'));
    },
    timeout: function() {
      setTimeout(this.resolve, 1000);
    }
  };

  before(function *() {
    var conn = yield amqp.connect('amqp://localhost');
    var channel = yield conn.createChannel();

    server = rpc.server({ channel: channel, api: api });
    client = rpc.client({ channel: channel, timeout: 100 });

    watcher = nigah(client);

    yield new Promise(function(resolve, reject) {
      client.on('connected', resolve);
      client.on('error', reject);
    });

    rpc.client.addMethods(client, Object.keys(api));
  });

  beforeEach(function() {
    watcher.resetHistory();
  });

  it('should call remote method', function(done) {
    client.add(1, 2)
      .then(function(res) {
        expect(res).to.equal(3);
      }).then(done, done).done();
  });

  it('should throw an error if remote method doesnt exist', function(done) {
    expect(function() {
      client.doesntExist();
    }).to.throw();

    client.request('doesntExist')
      .catch(function(err) {
        expect(err).to.be.an.instanceof(Promise.OperationalError);
      }).then(done).done();
  });

  it('should throw an error if remote method throws an error', function(done) {
    client.reject('remote error')
      .catch(function(err) {
        expect(err + '').to.contain('remote error');
      }).then(done).done();
  });

  it('should work with a single json argument', function(done) {
    client.json({ a: 'a' })
      .then(function(res) {
        expect(res).to.eql({ a: 'a' });
      }).then(done, done).done();
  });

  it('should work with multiple arguments', function(done) {
    client.args(1, 'a', { a: 'a' })
      .then(function(arr) {
        expect(arr[0]).to.equal(1);
        expect(arr[1]).to.equal('a');
        expect(arr[2]).to.eql({ a: 'a' });
      }).then(done, done).done();
  });

  describe('#timeout', function() {
    it('should timeout if server takes too long to process', function(done) {
      client.timeout().catch(function(err) {
        expect(err).to.be.an.instanceof(Promise.TimeoutError);
      }).then(done).done();
    });

    it('should remove promise from client', function(done) {
      client.timeout().catch(function() {
        expect(client.options.waiting).to.eql([]);
      }).then(done).done();
    });

    it('should handle server timeout gracefully', function(done) {
      client.timeout()
        .catch(function() {})
        .then(function() {
          return new Promise(function(resolve) {
            // wait for client to process invalid id
            client.on('invalid id', function() {
              expect(client.options.waiting).to.eql([]);
              resolve();
            });
          });
        }).then(done).done();
    });
  });
});
