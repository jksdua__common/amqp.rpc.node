'use strict';

var merge = require('lodash.merge');
var assert = require('assert');
var Promise = require('bluebird');
var stringify = require('json-stringify-safe');
var EventEmitter = require('events').EventEmitter;

var debug = require('debug')('amqp.rpc:server');

var slice = Array.prototype.slice;

/**
 * Options
 *
 * {Object}        [queue]                            RPC queue options
 * {String}        [queue.name=rpc]                   RPC queue name
 * {Object}        [queue.options={ durable:true }]   RPC queue options used to create the queue on the server
 *
 */
function RpcServer(options) {
  if (!(this instanceof RpcServer)) {
    return new RpcServer(options);
  }

  EventEmitter.call(this);

  this.options = merge({
    queue: {
      name: 'rpc',
      options: { durable: true }
    }
  }, options);

  debug('creating server with options: %j', this.options);

  this.api = this.options.api;
  this.queue = this.options.queue.name;
  this.channel = this.options.channel;
  assert(this.channel, 'Missing amqp channel');

  // create queue
  this.createQueue();
}

RpcServer.prototype = Object.create(EventEmitter.prototype);

RpcServer.prototype.respond = function(msg) {
  var self = this;
  self.handle(msg).then(function(res) {
    self.emit(self.EVENT.RESPONSE, res, msg);
    debug('sending response: %j', res);

    var resBuf = new Buffer(stringify(res));
    self.channel.sendToQueue(
      msg.properties.replyTo,
      resBuf,
      { correlationId: msg.properties.correlationId }
    );
    self.channel.ack(msg);
  }).catch(function(err) {
    self.emit(self.EVENT.ERROR, err);
  }).done();
};

RpcServer.prototype.handle = function(msg) {
  var self = this;

  return new Promise(function(resolve) {
    var req = JSON.parse(msg.content.toString());

    self.emit(self.EVENT.REQUEST, req, msg);
    debug('got request: %j', req);

    if (req.fn) {
      debug('got request for fn: %s, args: %j', req.fn, req.arguments);

      var fnPromise = new Promise(function(resolve, reject) {
        var fn = self.api[req.fn];
        assert(fn, 'Invalid method `' + req.fn + '`');

        // set promise as the context and call with arguments
        fn.apply({ resolve: resolve, reject: reject }, req.arguments);
      });

      fnPromise.then(function() {
        var args = slice.call(arguments);
        debug('resolving from fn with args: %j', args);

        resolve({ arguments: args });
      }).catch(function(err) {
        debug('rejecting from fn with err: %s', err);
        resolve({ error: { stack: err.stack, message: err.message } });
      });
    } else {
      var err = new Promise.OperationalError('Invalid request');
      debug('invalid request');
      resolve({ error: err.stack });
    }
  });
};

RpcServer.prototype.createQueue = function() {
  var self = this;
  var queue = this.queue;
  var options = this.options.queue.options;

  debug('creating queue: %s with options: %j', queue, options);
  this.channel.assertQueue(queue, options)
    .then(function() {
      return self.channel.consume(queue, function(msg) {
        self.respond(msg);
      });
    }).then(function() {
      debug('waiting for requests on queue: %s', queue);
      self.emit(self.EVENT.CONNECTED, self);
    }).done();
};

RpcServer.prototype.EVENT = {
  CONNECTED: 'connected',
  REQUEST: 'request',
  RESPONSE: 'response',
  ERROR: 'error'
};

module.exports = RpcServer;