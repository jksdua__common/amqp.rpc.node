'use strict';

var uuid = require('node-uuid');
var merge = require('lodash.merge');
var assert = require('assert');
var Promise = require('bluebird');
var stringify = require('json-stringify-safe');
var EventEmitter = require('events').EventEmitter;

var debug = require('debug')('amqp.rpc:client');

var slice = Array.prototype.slice;

/**
 * Options
 *
 * {Number}        [timeout=10000]                    Timeout in milliseconds, defaults to 10 seconds
 * {Object}        [queue]                            RPC queue options
 * {String}        [queue.name=rpc]                   RPC queue name
 * {Object}        [responseQueue]                    RPC queue options
 * {Object}        [responseQueue.options={exclusive:true}] Options used for creating the response queue
 * {Object}        [responseQueue.consumeOptions={noAck:true}] Options used for consuming the response queue
 *
 */
function RpcClient(options) {
  if (!(this instanceof RpcClient)) {
    return new RpcClient(options);
  }

  EventEmitter.call(this);

  this.options = merge({
    queue: {
      name: 'rpc'
    },
    responseQueue: {
      options: { exclusive: true },
      consumeOptions: { noAck: true }
    },
    timeout: 10000 // 10 seconds
  }, options);

  assert(this.options.channel, 'Missing amqp channel');

  debug('creating client with options: %j', this.options);

  // request queue
  this.options.requestQueue = this.options.queue.name;
  this.createResponseQueue();

  this.options.waiting = [];
}

RpcClient.prototype = Object.create(EventEmitter.prototype);

function addMethod(client, method) {
  return function() {
    return client.call(method, slice.call(arguments));
  };
}

RpcClient.addMethods = function(client, methods) {
  assert(client instanceof RpcClient, 'Invalid client given');
  assert(Array.isArray(methods), 'Invalid methods. Must be an array');

  debug('adding methods: %j', methods);

  for (var i = 0, len = methods.length; i < len; i += 1) {
    var method = methods[i];

    assert(!(method in client), '`' + method + '` method already exists');

    client[method] = addMethod(client, method);
  }
};

RpcClient.prototype.request = RpcClient.prototype.call = function(fn, args) {
  var self = this;
  var requestId = uuid();

  debug('got new request - fn: %s, reqId: %s', fn, requestId);

  return new Promise(function(resolve, reject) {
    self.options.waiting[requestId] = { resolve: resolve, reject: reject };

    var queue = self.options.requestQueue;
    var data = stringify({ fn: fn, arguments: args });
    var options = { correlationId: requestId, replyTo: self.options.responseQueue.name };

    self.emit(self.EVENT.REQUEST, data, options);
    self.options.channel.sendToQueue(queue, new Buffer(data), options);
    debug('sent request to queue: %s with data: %j, options: %j', queue, data, options);
  }).timeout(this.options.timeout).finally(function() {
    debug('removing promise for reqId: %s', requestId);

    delete self.options.waiting[requestId];
  });
};

RpcClient.prototype.respond = function(msg) {
  this.emit(this.EVENT.RESPONSE, msg);

  var id = msg.properties.correlationId;

  debug('responding to msg with resId: %s', id);

  if (id in this.options.waiting) {
    debug('found promise in `this.waiting` for resId: %s', id);

    var promise = this.options.waiting[id];

    var res = JSON.parse(msg.content.toString());
    debug('resId: %s, res: %j', id, res);

    if (res.error) {
      debug('rejecting resId: %s with error: %s', id, res.error);

      var clientError = new Promise.OperationalError(res.error.message);
      clientError.stack = res.error.stack;
      promise.reject(clientError);
    } else {
      debug('resolving resId: %s with args: %j', id, res.arguments);
      promise.resolve.apply(null, res.arguments);
    }
  } else {
    debug('invalid resId: %s', id);
    this.emit(this.EVENT.INVALID_ID, id, msg);
  }
};

// create response queue and subscribe to it
  // creates a random unique queue just for this client
RpcClient.prototype.createResponseQueue = function() {
  var responseQueue = this.options.responseQueue;

  debug('creating response queue with options: %j', responseQueue.options);

  var self = this;

  this.options.channel.assertQueue('', responseQueue.options)
    .then(function(qok) {
      var queue = qok.queue;
      responseQueue.name = queue;

      debug('got queue: %s, consuming with options: %j', queue, responseQueue.consumeOptions);

      return self.options.channel.consume(queue, function(msg) { self.respond(msg); }, responseQueue.consumeOptions)
        .then(function() {
          debug('waiting for responses on queue: %s', queue);
          self.emit(self.EVENT.CONNECTED, self);
        });
    }).catch(function(err) {
      self.emit(self.EVENT.ERROR, err);
    }).done();
};

RpcClient.prototype.EVENT = {
  CONNECTED: 'connected',
  REQUEST: 'request',
  RESPONSE: 'response',
  INVALID_ID: 'invalid id',
  ERROR: 'error'
};

module.exports = RpcClient;
